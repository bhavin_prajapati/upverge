/*
 * client/js/collections/companies.js
 */

/* global define */

define([
  'backbone',
  'backbone-pageable',
  'models/listitem'
], function (Backbone, BackbonePageable, ListItem) {
  'use strict';

  return Backbone.Collection.extend({
    model: ListItem,
    url: '/companies'
  });

  /*
  return Backbone.PageableCollection.extend({
    model: ListItem,
    mode: 'infinite',
    url: '/companies',

    // Initial pagination states
    state: {
      pageSize: 10,
      sortKey: 'updated',
      order: 1
    },

    // You can remap the query parameters from ``state`` keys from the default
    // to those your server supports. Setting ``null`` on queryParams removed them
    // from being appended to the request URLs.
    queryParams: {
      totalPages: null,
      totalRecords: null,
      sortKey: 'sort',
      order: 'direction',
      directions: {
        '-1': 'asc',
        '1': 'desc'
      }
    }
  });
  */
});
