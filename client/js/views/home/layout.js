/*
 * client/js/views/home/layout.js
 */

/* global define */

define([
  'backbone',
  'collections/companies',
  'views/home/companies',
  'hbs!templates/home/layout'
], function (Backbone, Companies, CompaniesView, layoutTpl) {
  'use strict';

  return Backbone.Marionette.Layout.extend({
    template: layoutTpl,

    regions: {
      mainRegion: '#layout'
    },

    onShow: function () {
      var companies = new Companies();
      this.mainRegion.show(new CompaniesView({
        collection: companies
      }));
    }
  });
});
