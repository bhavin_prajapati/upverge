/*
 * client/js/views/home/companies.js
 */

/* global define */

define([
  'backbone',
  'handlebars',
  'hbs!templates/home/company',
  'hbs!templates/home/companies'
], function (Backbone, Handlebars, companyTpl, companiesTpl) {
  'use strict';

  var LoadingView = Backbone.Marionette.ItemView.extend({
    template: Handlebars.compile(
      '<div style="height: 50px;">' +
      '<div style="margin-left: auto;margin-right: auto;width: 25px;">' +
      '<i class="icon-spinner icon-spin icon-2x"></i>' +
      '</div>' +
      '</div>'
    )
  });

  var EmptyView = Backbone.Marionette.ItemView.extend({
    template: Handlebars.compile('<em>N/A</em>')
  });

  var CompanyView = Backbone.Marionette.ItemView.extend({
    template: companyTpl,
    itemViewContainer: 'ul',
    tagName: 'tr'
  });

  return Backbone.Marionette.CompositeView.extend({
    template: companiesTpl,
    itemViewContainer: 'tbody.companies',
    itemView: CompanyView,
    emptyView: EmptyView,

    initialize: function() {
      this.listenTo(this.collection, 'request', this.onRequest);
      this.listenTo(this.collection, 'sync', this.onSync);
      this.collection.fetch();
    },

    onRequest: function () {
      this.emptyView = LoadingView;
      this.render();
    },

    onSync: function () {
      this.emptyView = EmptyView;
      this.render();
    }
  });

});
