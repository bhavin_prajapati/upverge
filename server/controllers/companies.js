/*
 * server/controllers/companies.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');
//var http = require('http');
//var jsonxml = require('jsontoxml');
var app = require('../app');
//var app = require('../../app');

function _getContent(cb) {
    /*
    var page = queryObject.page;
    var perPage = queryObject.per_page;
    var sort = queryObject.sort;
    var direction = queryObject.direction;
    var skip = per_page * (page - 1);
    console.log('Query params page:' + page);
    console.log('Query params per_page:' + perPage);
    console.log('Query params sort:' + sort);
    console.log('Query params direction:' + direction);
    console.log('skip:' + skip);
    */

    //if (page != null && per_page != null && sort != null && direction != null) {
      var query = app.models.Company.find(
        {},
        { 'companyname':1, 'amount':1, 'url':1, 'logourl':1},
        {
          sort: {
            amount:-1,
            updatedAt: -1
          }
        }
      );

      query.exec(function (err, items) {
        console.log('err:' + err);
        console.log('items:' + items);

        if (items) {
          var i, n = items.length;
          console.log('length: ' + n);
          for (i = 0; i < n; i++) {
            var amt = items[i].amount.toFixed(2);
            var item = {
              'index' : i+1,
              'companyname' : items[i].companyname,
              'amount' : '$ ' + amt,
              'url' : items[i].url,
              'logourl' : items[i].logourl,
            };
            items[i] = item;
            console.log(items[i]);
          }
          return cb(null, items);
        }
      });
    //}
    //return cb(null, '{}');
    /*
    app.models.Company.find(
    {},
    { 'companyname':1, 'amount':1, 'url':1, 'logourl':1},
    {
    sort:{
        amount:-1,
        updatedAt: -1
      }
    },
    function (err, items) {
      if (items) {
        //_.each(items, function (item, i) {
        //  item.index = i;
        //});
        return cb(null, items);
      }
    }).skip(skip).limit(per_page);
    */
    }

function LIST(req, cb) {
  //var queryObject = url.parse(req.url,true).query;
  //console.log('Query params:' + queryObject.page);
  //_getContent(cb, queryObject);
  _getContent(cb);
}

function GET(req, id, cb) {
  //var queryObject = url.parse(req.url,true).query;
  //console.log('Query params:' + queryObject.page);
  //_getContent(cb, queryObject);
  _getContent(cb);
}

function POST(req, cb) {
  cb('invalid method - ' + req.method);
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
