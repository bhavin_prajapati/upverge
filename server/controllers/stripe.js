/*
 * server/controllers/api/top.js
 */

'use strict';

//var fs = require('fs'),
//    path = require('path');
//var http = require('http');

/*IMPORTANT: Make sure you fill in your secret API key into the stripe module.*/
var stripe = require('stripe')('sk_test_etBIlRYSPlM7OPHXuy1irChq');
var nodemailer = require('nodemailer');
var app = require('../app');

function LIST(req, cb) {
  cb('invalid method - ' + req.method);
}

function GET(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function sendEmail( options ){
  // Create a SMTP transport object
  var transport = nodemailer.createTransport('SMTP', {
    service: 'Gmail', // use well known service.
                        // If you are using @gmail.com address, then you don't
                        // even have to define the service name
    auth: {
        user: 'bhavin.prajapati@vingosoftware.com',
        pass: 'Welcome2014!!'
      }
    });

  console.log('SMTP Configured');

  // Message object
  var message = {

      // sender info
      from: '"Upverge" <bhavin.prajapati@vingosoftware.com>',

      // Comma separated list of recipients
      to: '"' + options.firstname + ' ' + options.lastname + '" <' + options.email + '>',

      // Subject of the message
      subject: options.subject, //

      headers: {
          'X-Laziness-level': 1000
        },

        // plaintext body
        //text: 'Hello to myself!',

        // HTML body
        html: options.html,

      };

  console.log('Sending Mail');
  transport.sendMail(message, function(error){
      if(error){
        console.log('Error occured');
        console.log(error.message);
        return;
      }
      console.log('Message sent successfully!');

      // if you don't want to use this transport object anymore, uncomment following line
      //transport.close(); // close the connection pool
    });
}

function POST(request, cb) {
  //Object POSTed to server contains the Stripe token from Stripe's servers.
  var transaction = request.body;
  var stripeToken = transaction.stripeToken;
  var isfree = transaction.isfree;
  var companyname = transaction.companyname;
  var companyemail = transaction.companyemail;
  var amount = transaction.amount;
  if ( amount === 0 ) {
    isfree = 'true';
  }
  if( isfree === 'true' ) {
    amount = 0;
  } else {
    amount = transaction.amount;
  }
  var url = transaction.url;
  var logourl = transaction.logourl;
  var address1 = transaction.address1;
  var address2 = transaction.address2;
  var city = transaction.city;
  var stateprovince = transaction.stateprovince;
  var country = transaction.country;
  var zipcodepostalcode = transaction.zipcodepostalcode;
  var phonenumber = transaction.phonenumber;
  var sloganmessage = transaction.sloganmessage;
  var fullname = transaction.fullname;
  var email = transaction.email;
  var registrationNumber = Math.floor(Math.random() * 1000000000);

  console.log('transaction: ' + transaction);
  console.log('stripeToken:' + stripeToken);
  console.log('isfree:' + isfree);
  console.log('companyname: ' + companyname);
  console.log('companyemail: ' + companyemail);
  console.log('amount: ' + amount);
  console.log('url: ' + url);
  console.log('logourl: ' + logourl);
  console.log('address1: ' + address1);
  console.log('address2: ' + address2);
  console.log('city: ' + city);
  console.log('stateprovince: ' + stateprovince);
  console.log('country: ' + country);
  console.log('zipcodepostalcode: ' + zipcodepostalcode);
  console.log('phonenumber: ' + phonenumber);
  console.log('sloganmessage: ' + sloganmessage);
  console.log('fullname: ' + fullname);
  console.log('email: ' + email);
  console.log('registrationNumber: ' + registrationNumber);

  var res = cb(null,'getres');

  if( isfree === 'true' ){
    console.log('Adding company with free = true');
    // Registers company
    app.models.Company.findOne({
      'companyname': companyname
    }, function (err, company) {

      console.log('err:' + err);
      console.log('company:' + company);

      if (err) { cb(null, JSON.stringify(err)); }
      if (company) {
        res.render('purchase/index', {
          confirm: 'false'
        });
      }
      new app.models.Company({
        'companyname': companyname,
        'companyemail': companyemail,
        'amount': amount,
        'url': url,
        'logourl': logourl,
        'address1': address1,
        'address2': address2,
        'city': city,
        'stateprovince': stateprovince,
        'country': country,
        'zipcodepostalcode': zipcodepostalcode,
        'phonenumber': phonenumber,
        'sloganmessage': sloganmessage
      }).save(function (err, company) {
        if (err) {
          console.log('Error registering company:' + err);
          res.render('purchase/index', {
            confirm: 'false'
          });
        } else {
          console.log('Successfully registered company!');
          console.log(company);
          res.render('purchase/index', {
            confirm: 'true'
          });
        }
      });
    });
  } else {
    console.log('Adding company with free = false and amount = ' + amount);
    //For now, charge $10.00 to the customer.
    var charge =
    {
      amount: amount*100, //Charge is in cents
      currency: 'USD',
      card: stripeToken //Card can either be a Stripe token, or an object containing credit card properties.
    };

    stripe.charges.create(charge,
        //All stripe module functions take a callback, consisting of two params:
        // the error, then the response.
        function(err, charge)
        {
          if(err)
          {
            console.log(err);
          }
          else
          {
            if(charge.paid) {
              console.log('charge.paid = true');
              sendEmail({
                firstname: fullname.split(' ')[0],
                lastname: fullname.split(' ')[1],
                email: email,
                subject: 'Upverge Registration #' + registrationNumber,
                html: '<h2>Upverge Registration #' + registrationNumber + '</h2><p>Your registration to Upverge has been successfully completed.</p>'
              });

              sendEmail({
                firstname: 'Bhavin',
                lastname: 'Prajapati',
                email: 'bhavin.prajapati@vingosoftware.com',
                subject: 'Upverge Registration #' + registrationNumber,
                html: '<p>Registration Information</p><p>isfree: ' + isfree + '</p><p>companyname: ' + companyname + '</p><p>companyemail: ' + companyemail + '</p><p>amount: ' + amount + '</p><p>url: ' + url + '</p><p>logourl: ' + logourl + '</p><p>address1: ' + address1 + '</p><p>address2: ' + address2 + '</p><p>city: ' + city + '</p><p>stateprovince: ' + stateprovince + '</p><p>country: ' + country + '</p><p>zipcodepostalcode: ' + zipcodepostalcode + '</p><p>phonenumber: ' + phonenumber + '</p><p>sloganmessage: ' + sloganmessage + '</p>'
              });
            } else {
              console.log('charge.paid = false');
            }

            // Registers company
            app.models.Company.findOne({
              'companyname': companyname
            }, function (err, company) {
              if (err) {
                res.render('purchase/index', {
                  confirm: 'false'
                });
              }
              if (company) {
                res.render('purchase/index', {
                  confirm: 'false'
                });
              }
              new app.models.Company({
                'companyname': companyname,
                'companyemail': companyemail,
                'amount': amount,
                'url': url,
                'logourl': logourl,
                'address1': address1,
                'address2': address2,
                'city': city,
                'stateprovince': stateprovince,
                'country': country,
                'zipcodepostalcode': zipcodepostalcode,
                'phonenumber': phonenumber,
                'sloganmessage': sloganmessage
              }).save(function (err, company) {
                if (err) {
                  console.log('Error registering company:' + company);
                  res.render('purchase/index', {
                    confirm: 'false'
                  });
                }
              });
            });

            console.log('Successfully registered and charge sent to Stripe!');
            res.render('purchase/index', {
              confirm: 'true'
            });
          }
        }
    );
  }
}

function PUT(req, id, cb) {
  cb('invalid method - ' + req.method);
}

function DELETE(req, id, cb) {
  cb('invalid method - ' + req.method);
}

// Public API
exports.__filename = __filename;
exports.LIST = LIST;
exports.GET = GET;
exports.POST = POST;
exports.PUT = PUT;
exports.DELETE = DELETE;
