/*
 * server/models/ListItem.js
 */

'use strict';

//var _ = require('lodash');
var mongoose = require('mongoose');

var plugin = mongoose.customPlugin;

// Schema
var schema = new mongoose.Schema({
	companyname: { type: String, required: true },
  companyemail: { type: String, required: true },
	amount: { type: Number, required: true },
	url: { type: String, required: true },
  logourl: { type: String, required: true },
  address1: { type: String, required: true },
  address2: { type: String },
  city: { type: String },
  stateprovince: { type: String },
  country: { type: String },
  zipcodepostalcode: { type: String },
  phonenumber: { type: String },
  sloganmessage: { type: String },
});

// Indexes
schema.path('amount').index();

// Plugins
schema.plugin(plugin.findOrCreate);
schema.plugin(plugin.timestamps);

// ListItem
var model = mongoose.model('Company', schema);

// Public API
exports = module.exports = model;
